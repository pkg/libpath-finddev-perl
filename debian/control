Source: libpath-finddev-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Florian Schlichting <fsfs@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper (>= 10),
               perl
Build-Depends-Indep: libclass-tiny-perl,
                     libpath-isdev-perl,
                     libpath-tiny-perl,
                     perl (>= 5.19.6) | libtest-simple-perl (>= 1.001002),
                     libsub-exporter-perl
Standards-Version: 4.1.3
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-perl/packages/libpath-finddev-perl.git
Vcs-Git: https://anonscm.debian.org/git/pkg-perl/packages/libpath-finddev-perl.git
Homepage: https://metacpan.org/release/Path-FindDev

Package: libpath-finddev-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libclass-tiny-perl,
         libpath-isdev-perl,
         libpath-tiny-perl,
         libsub-exporter-perl
Description: Perl module to find a development source tree somewhere in an upper hierarchy
 Path::FindDev provides an easy and platform-independent way to find the
 root of a development source tree in some parent directory, irrespective
 of which test you put it in, and regardless of what $CWD happens to be
 when you call it.  Path::FindDev is mostly a glue layer around
 Path::IsDev with a few directory walking tricks.
